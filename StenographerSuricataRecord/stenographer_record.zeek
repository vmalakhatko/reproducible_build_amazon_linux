@load base/utils/exec

function write_to_pipe(pipe: string, message: string) : count
	{
	local cmd = Exec::Command($cmd=fmt("echo \"(%s)\" > %s &", message, pipe));
	when ( local result = Exec::run(cmd) )
		{
		return result$exit_code;
		}
	}
