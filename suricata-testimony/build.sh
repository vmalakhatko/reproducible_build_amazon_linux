#!/bin/bash -xe

. ../utils.sh

parse_args "${@}"

readonly SURICATA_TAR_PATH="${RPM_SOURCES}/suricata-${VERSION}.tar.gz"

# For `testimony` version testimony and libhtp should be downloaded separately.
# For other versions libhtp is already included into an archive.
load_repo "${SURICATA_GIT}" "${RPM_BUILD}/suricata-${VERSION}" "${SURICATA_COMMIT}" "${SURICATA_BRANCH}"
load_repo "${TESTIMONY_GIT}" "${RPM_BUILD}/suricata-${VERSION}/testimony" "${TESTIMONY_COMMIT}"
load_repo "${LIBHTP_GIT}" "${RPM_BUILD}/suricata-${VERSION}/libhtp" "${LIBHTP_COMMIT}"

load_repo "${SURICATA_TESTIMONY_PLUGIN_GIT}" "${RPM_BUILD/suricata-testimony-plugin}"

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog
export SOURCE_DATE_EPOCH="$(head -1 "${RPM_BUILD}/suricata-${VERSION}/ChangeLog" | rev | cut -d' ' -f1 | rev | date -f- +%s)"
export SURICATA_CONFIGURE_FLAGS="${BUILD_ARGS_TAIL[@]}"

