%define _buildhost reproducible
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}

# libtestimony is already shipped with here, in RPM, but rpmbuild treats it as an external dependency.
# So here it is excluded manually.
#%define __requires_exclude ^.*testimony.*$

Name:           suricata-testimony
Version:        %{_version}
Release:        0

Summary:        Suricata-Testimony plugin
License:        GPL-2.0-only
URL:            https://github.com/MalakhatkoVadym/suricata-testimony-plugin
BuildRoot:      %{_tmppath}/%{name}-root
# Requires:       suricata
# BuildRequires:  suricata

%description
This plugin provides native Testimony support for Suricata. Testimony is a single-machine, multi-process architecture for sharing AF_PACKET data across processes. (https://github.com/google/testimony).
# Setup step is disabled because it requres source archive to be available.
# In case of the usage of git version it is pointless.
%prep
#%setup -q
%build

cd %{_builddir}

echo "Building Testimony"

cd testimony/c
make
objcopy --enable-deterministic-archives libtestimony.a
make install

cd %{_builddir}/suricata-testimony

echo "Building Suricata-Testimony plugin"

# ./configure --install-root="${RPM_BUILD_ROOT}/%{_prefix}/lib/suricata/plugins/"

# find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

# cd build
CPPFLAGS="-I%{_builddir}/suricata/src" make -j`nproc`
cd -

%install

cd %{_builddir}/suricata-testimony

# install -D -m 644 %{_builddir}/testimony/c/libtestimony.so -t "${RPM_BUILD_ROOT}/%{_libdir}"

# Install Suricata-Testimony plugin
# cd build
make install
cd -

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/suricata-testimony/files.list

%files -f %{_builddir}/suricata-testimony/files.list


