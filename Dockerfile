# Describes build environment for Suricata and Zeek reproducible builds

# To make this environment deterministic and reproducible,
# all dependencies including container image should have explicit versions set.

FROM amazonlinux:2

COPY ./rpms ./


# Centos Stream
# RUN dnf -y install centos-release-stream
# RUN dnf -y swap centos-{linux,stream}-repos
# RUN dnf -y distro-sync
# RUN cat /etc/centos-release

#RUN dnf clean all && rm -r /var/cache/dnf  && dnf upgrade -y && dnf update -y & echo hostname
#RUN yum search --showduplicates zlib && false
#RUN dnf -y install dnf-plugins-core
#RUN dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
#RUN dnf config-manager --set-enabled PowerTools

RUN amazon-linux-extras install epel -y

#RUN yum-config-manager --enable epel

RUN yum -y install \
    libxcrypt-devel \
    glibc \
    glibc-headers \
    glibc-devel \
    libgomp \
    libgcc \
    libpkgconf \
    pkgconf-m4 \
    pkgconf \
    pkgconf-pkg-config \
    libstdc++ \
    cmake \
    emacs-filesystem \
    cmake-data \
    cmake-filesystem \
    cmake-rpm-macros \
    libstdc++-devel \
    libuv 


#RUN dnf -y install dnf-plugins-core

#RUN dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

# RUN dnf config-manager --set-enabled powertools

# Suricata build deps
RUN yum -y install cargo gcc jansson-devel nss nss-devel rust libcap-ng-devel lz4-devel \
    make pcre-devel python3-pyyaml zlib zlib-devel file-devel \
    libmaxminddb-devel \
    libnet-devel \
    libnetfilter_queue-devel \
    libpcap-devel \
    libyaml-devel \
    lua-devel 

# Zeek build deps
RUN yum -y install \
    bison cmake flex gcc-c++ python36-devel swig openssl-devel

# Common and RPM build deps
RUN yum -y install \
    wget rpm-build rpmdevtools git which libtool

# RUN yum -y --nogpgcheck localinstall \
#     nfpm_amd64.rpm
RUN curl -sfL https://install.goreleaser.com/github.com/goreleaser/nfpm.sh | sh

ENV PATH="/root/.cargo/bin:${PATH}"

RUN cargo install --force cbindgen --version 0.14.1

# `cbindgen` is installed here, so this dir should be added to PATH

RUN yum -y install leveldb-devel leveldb

RUN yum -y install \
    libaio-devel \
    libseccomp-devel \
    snappy \
    snappy-devel

RUN yum -y install golang libcurl-devel

ENV BUILD_DEST /root/dest
RUN mkdir -p "${BUILD_DEST}"

RUN wget https://dl.google.com/go/go1.13.4.linux-amd64.tar.gz

RUN tar -C /usr/local -xf go1.13.4.linux-amd64.tar.gz

RUN export PATH=$PATH:/usr/local/go/bin

#RUN source ~/.bash_profile

RUN yum -y install cmake3 python-devel

WORKDIR /root

COPY ./utils.sh utils.sh

COPY ./entrypoint.sh entrypoint.sh

COPY ./suricata ./suricata

COPY ./suricata-hassh ./suricata-hassh

COPY ./zeek ./zeek

COPY ./zeek-testimony ./zeek-testimony

COPY ./testimony ./testimony

COPY ./stenographer ./stenographer

COPY ./suricata-testimony-plugin ./suricata-testimony-plugin

COPY ./suricata-stenographer-plugin ./suricata-stenographer-plugin

COPY ./dependency-downloader ./dependency-downloader
