#!/bin/bash -xe

dnf clean packages
# testimony dependencies
mkdir -p "${BUILD_DEST}"/testimony
# yum --downloadonly reinstall -y --downloaddir "${BUILD_DEST}" jansson nss libnet libnetfilter_queue pkg-config


dnf clean packages
# suricata dependencies
mkdir -p "${BUILD_DEST}"/suricata
dnf --downloadonly reinstall -y --downloaddir "${BUILD_DEST}/suricata" \
    jansson \
    nss \
    libnet \
    libnetfilter_queue \
    pkg-config


dnf clean packages
# stenographer dependencies 
mkdir -p "${BUILD_DEST}"/stenographer
dnf --downloadonly reinstall -y --downloaddir "${BUILD_DEST}"/stenographer libaio leveldb snappy libpcap libseccomp curl jq shadow-utils

dnf clean packages
# zeek dependencies
mkdir -p "${BUILD_DEST}"/zeek
dnf --downloadonly reinstall -y --downloaddir "${BUILD_DEST}"/zeek flex bison libpcap-devel openssl-devel python3-devel swig zlib-devel

# testing libraries
mkdir -p "${BUILD_DEST}"/testing
yum --downloadonly install -y --downloaddir "${BUILD_DEST}"/testing python3-attrs python3-pluggy python3-py python3-pytest
pip3 download -d "${BUILD_DEST}"/testing attrs pytest importlib_metadata iniconfig packaging pluggy py pyparsing six testinfra toml zipp

# suricata-stenographer plugin
mkdir -p "${BUILD_DEST}"/suricata-stenographer
dnf --downloadonly reinstall -y --downloaddir "${BUILD_DEST}"/suricata-stenographer \
    libcurl-devel \
    lz4-devel

# syslog-ng downloading
dnf --downloadonly install -y --downloaddir "${BUILD_DEST}"/testimony \
    ivykis \
    syslog-ng