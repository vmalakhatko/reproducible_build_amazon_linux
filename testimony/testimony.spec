%define _buildhost reproducible 
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}

Name:           testimony
Version:        %{_version}
Release:        0

Summary:        Testimony is a single-machine, multi-process architecture for sharing AF_PACKET data across processes.
License:        GPL-2.0-only
URL:            https://github.com/google/testimony
#Source0:        suricata-% # {_version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-root
Requires:       jansson nss libnet libnetfilter_queue pkg-config

%description
Testimony is a single-machine, multi-process architecture for sharing AF_PACKET data across processes.

# Setup step is disabled because it requres source archive to be available. 
# In case of the usage of git version it is pointless.
%prep
#%setup -q
%build

cd %{_builddir}/suricata-%{_version}

CFG_FLAGS=(${SURICATA_CONFIGURE_FLAGS})

find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

make -j`nproc`

%install

export QA_RPATHS=0x0001

[ "${RPM_BUILD_ROOT}" != "/" ] && rm -rf "${RPM_BUILD_ROOT}"

cd %{_builddir}/testimony-%{_version}

install -D -m 644 %{_builddir}/go/testimonyd -t "${RPM_BUILD_ROOT}/%{_bindir}"
install -D -m 644 %{_builddir}/c/libtestimony.{a,so} -t "${RPM_BUILD_ROOT}/%{_libdir}"
install -D -m 644 %{_builddir}/c/testimony.h -t "${RPM_BUILD_ROOT}/%{_includedir}"

make DESTDIR="${RPM_BUILD_ROOT}" install

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/suricata-%{_version}/files.list

%files -f %{_builddir}/suricata-%{_version}/files.list

