import json
import tools
import sys
import os

config_file_name = 'config'
config_default_path = '/etc/'
default_configuration = {
    "Threads": [
        { "PacketsDirectory": "/data/stenographer/PKT0/"
        , "IndexDirectory": "/data/stenographer/IDX0/"
        , "MaxDirectoryFiles": 30000
        , "DiskFreePercentage": 1
        }
    ]
    , "StenotypePath": "/usr/bin/stenotype"
    , "TestimonySocket": "/tmp/testimony.sock"
    , "Port": 1234
    , "Host": "127.0.0.1"
    , "Flags": ["-v"]
    , "CertPath": "/etc/nsm/stenographer/certs"
    }

class StenographerConfig:    
    def generate_config_file(self, config_file, stenotype_path, interface, testimony_socket, port, host, cert_path, fanout_size, packet_index_dir, uid, gid, max_packet_files, disk_percentage):
        config = {}
        if not testimony_socket and not interface:
            print(f"{tools.bcolors.FAIL} Interface or Testimony socket are not defined \n Please, define --interface or --testimony_socket {tools.bcolors.ENDC}")
            sys.exit(0)

        if testimony_socket and interface:
            print(f"{tools.bcolors.FAIL} Interface or Testimony socket are both defined \n Please, define --interface= or --testimony_socket {tools.bcolors.ENDC}")
            sys.exit(0)

        config['StenotypePath'] = stenotype_path
        if interface:
            config['Interface'] = interface
        if testimony_socket:
            config['TestimonySocket'] = testimony_socket
        config['Port'] = port
        config['Host'] = host
        config['Flags'] = []
        config['CertPath'] = cert_path

        threads_config = {}

        dir = threads_config['IndexDirectory'] = threads_config['PacketsDirectory'] = packet_index_dir
        threads_config['MaxDirectoryFiles'] = max_packet_files
        threads_config['DiskFreePercentage'] = disk_percentage

        config.pop('Threads', None)
        config['Flags'].append(f"--dir={dir}")
        config['Flags'].append(f"--uid={uid}")
        config['Flags'].append(f"--gid={gid}")
        config['Threads'] = []
        for i in range(0, fanout_size):
            current_threads_config = threads_config.copy()
            current_threads_config['PacketsDirectory'] = threads_config['PacketsDirectory'] + "PKT" + str(i)
            current_threads_config['IndexDirectory'] = threads_config['IndexDirectory'] + "IDX" + str(i)
            config["Threads"].append(current_threads_config)
        
        print("Generated config({}): \n {}".format(config_file, json.dumps(config, indent=4)))

        try:
            with open(config_file, 'w') as outfile:
                json.dump(config, outfile)
            
            os.system("cp " + os.getcwd() + "/stenographer.service /usr/lib/systemd/system/")
            os.system("systemctl daemon-reload")
            os.system("systemctl enable stenographer")
            os.system("systemctl start stenographer")
        except PermissionError:
            print(f"{tools.bcolors.FAIL}You don't have permission to create or edit file {config_file} {tools.bcolors.ENDC}")

