import json
import tools
import yaml
import os
import sys

config_file_name = 'suricata.yaml'
config_default_path = '/etc/'

testimony_plugin_library_path = '/usr/'
testimony_plugin_library = 'source-testimony.so'

stenographer_plugin_library_path = '/usr/'
stenographer_plugin_library = 'eve-stenographer.so'

suricata_config_file_path = 'suricata.yaml'

testimony_default_configuration = {
    "plugins": ["/path/to/testimony/library/source-testimony.so"],
    "testimony": [{
        "socket": "/tmp/testimony.sock",
        "fanout-size": 4}]
}

def lines_that_contain(string, fp):
    return [line for line in fp if string in line]

def add_leading_spaces(stream):
    stream = stream[2:]
    return stream


class SuricataConfig:    
    def generate_config_file(self, config_file, testimony_lib, stenographer_lib,
                             testimony_socket, fanout_size, filename, pcap_dir, cert_dir,
                             command_pipe, before_time, after_time,
                             compression, no_overlapping, clean_up, cleanup_script,
                             expiry_time, min_disk_space_left):
        config = {}
        with open(os.getcwd() + "/" + suricata_config_file_path) as suricata_config_file:
            try:
                new_config = suricata_config_file.readlines()
            except yaml.YAMLError as exc:
                print(exc)
                return
        
        stenographer_config = {}
        stenographer_config["enabled"] = True
        stenographer_config["filename"] = filename
        stenographer_config["pcap-dir"] = pcap_dir
        stenographer_config["cert-dir"] = cert_dir
        stenographer_config["command-pipe"] = command_pipe
        stenographer_config["before-time"] = before_time
        stenographer_config["after-time"] = after_time
        stenographer_config["compression"] = True if compression in {"yes", "True"} else False
        stenographer_config["no-overlapping"] = True if no_overlapping in {"yes", "True"} else False
        if clean_up in {"yes", "True"}:
            stenographer_config["cleanup"] = {}
            stenographer_config["cleanup"]["enabled"] = True
            stenographer_config["cleanup"]["script"] = cleanup_script
            stenographer_config["cleanup"]["expiry-time"] = expiry_time
            stenographer_config["cleanup"]["min-disk-space-left"] = min_disk_space_left
        
        stenographer_config_main = {}
        stenographer_config_main["stenographer-plugin"] = stenographer_config
        
        with open(config_file, 'w') as outfile:
            for line in new_config:
                outfile.write(line)
                if 'filename: eve.json' in line:
                    yamlstrconfig = yaml.safe_dump(stenographer_config_main, default_style=None, default_flow_style=False, allow_unicode=True)
                    for line in yamlstrconfig.splitlines():
                        outfile.write('      ' + line + '\n')

        config["plugins"] = []
        config["plugins"].append(testimony_lib)
        config["plugins"].append(stenographer_lib)

        config["testimony"] = {}
        config["testimony"][testimony_socket] = {
            "fanout-size": fanout_size
        }
        
        print("Generated config({}): \n".format(config_file))
        yaml.safe_dump(stenographer_config_main, sys.stdout, default_flow_style=False, default_style=None, allow_unicode=True)
        print()
        yaml.safe_dump(config, sys.stdout, default_flow_style=False, default_style=None, allow_unicode=True)

        try:
            with open(config_file, 'a') as outfile:
                yaml.safe_dump(config, outfile, default_flow_style=False, default_style=None, allow_unicode=True)

            os.system("cp " + os.getcwd() + "/suricata.service /usr/lib/systemd/system/")
            os.system("systemctl daemon-reload")
            os.system("systemctl enable suricata")
            os.system("systemctl start suricata")
        except PermissionError:
            print(f"{tools.bcolors.FAIL}You don't have permission to create or edit file {config_file} {tools.bcolors.ENDC}")
