%define _buildhost reproducible 
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}

# libtestimony is already shipped with zeek here, in RPM, but rpmbuild treats it as an external dependency.
# So here it is excluded manually.
%define __requires_exclude ^.*testimony.*$

Name:           zeek
Version:        %{_version}
Release:        0

Summary:        Zeek
License:        GPL-2.0-only
URL:            https://github.com/zeek/zeek.git
BuildRoot:      %{_tmppath}/%{name}-root

%description
The Zeek Network Security Monitor.

# Setup step is disabled because it requres source archive to be available. 
# In case of the usage of git version it is pointless.
%prep
# %setup -q
%build

cd %{_builddir}/zeek-%{_version}

CFG_FLAGS=(${ZEEK_CONFIGURE_FLAGS})

FULL_LIB_DIR="%{_libdir}"
LIBDIR="${FULL_LIB_DIR#'/usr/'}"

./configure --prefix="%{_prefix}" \
    --conf-files-dir="%{_sysconfdir}/nsm/zeek" \
    --localstatedir="%{_var}" \
    --build-type=release ${CFG_FLAGS[@]}

find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

make -j`nproc`

%install

# RPATH is used bu Zeek and can't be safely disabled.
# Thus, disable checks for rpath. They produce errors and fail build.
export QA_RPATHS=0x0001

[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

cd %{_builddir}/zeek-%{_version}

make DESTDIR=$RPM_BUILD_ROOT install

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/zeek-%{_version}/files.list

%files -f %{_builddir}/zeek-%{_version}/files.list

