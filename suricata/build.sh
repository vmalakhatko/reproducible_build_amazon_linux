#!/bin/bash -xe

. ../utils.sh

parse_args "${@}"

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

# For `testimony` version testimony and libhtp should be downloaded separately.
# For other versions libhtp is already included into an archive.
readonly SURICATA_TAR_PATH="${RPM_SOURCES}/suricata-${VERSION}.tar.gz"

wget -P "${RPM_SOURCES}/" "https://github.com/OISF/suricata/archive/suricata-${VERSION}.tar.gz" || {
    error "Failed to download Suricata version '${VERSION}'"
}

tar -C "${RPM_BUILD}/" -xzf "${SURICATA_TAR_PATH}"  || {
    error "Failed extract Suricata version '${VERSION}'"
}

# load_repo "${SURICATA_GIT}" "${RPM_BUILD}/suricata-${VERSION}" "${SURICATA_COMMIT}" "${SURICATA_BRANCH}"
load_repo "${LIBHTP_GIT}" "${RPM_BUILD}/suricata-suricata-${VERSION}/libhtp" "${LIBHTP_COMMIT}"


# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog
export SOURCE_DATE_EPOCH="$(head -1 "${RPM_BUILD}/suricata-suricata-${VERSION}/ChangeLog" | rev | cut -d' ' -f1 | rev | date -f- +%s)"
export SURICATA_CONFIGURE_FLAGS="${BUILD_ARGS_TAIL[@]}"

build_rpm suricata.spec