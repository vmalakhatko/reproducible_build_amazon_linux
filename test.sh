#!/bin/bash -e

function error() {
    1>&2 echo "${@}"
    exit 1
}

# Tries to install RPM package
function test_installation() 
{
    target="${1}"

    for rpm in ./out/${target}-*.rpm
    do
        local name="$(basename ${rpm})"
        echo "Testing installation of ${name}"

        [[ "${name}" == zeek-testimony* ]] && continue

        docker run -it -v "${PWD}/out":/rpms centos:centos8@sha256:9e0c275e0bcb495773b10a18e499985d782810e47b4fce076422acb4bc3da3dd /bin/bash -c "yum --nogpgcheck install -y which /rpms/${name} && which ${target}" || {
            error "Failed to install and locate ${target}!"
        }
        
        echo "${target} installed and located successfully"
    done
}

function test_installation_zt() 
{
    echo "Testing installation Zeek-Testimony"
    
    docker run -it -v "${PWD}/out":/rpms centos:centos8@sha256:9e0c275e0bcb495773b10a18e499985d782810e47b4fce076422acb4bc3da3dd /bin/bash -c "yum --nogpgcheck install -y which /rpms/zeek-3.1.1-0.x86_64.rpm /rpms/zeek-testimony-latest-0.x86_64.rpm && zeek -NN Zeek::Testimony" || {
        error "Failed to install and locate Zeek-Testimony!"
    }
}

mkdir -p out

echo "Building Suricata from official repo"
./build.sh suricata 5.0.2 --enable-lua --enable-nfqueue

echo "Building Suricata with Testimony from Github"
./build.sh suricata testimony --testimony-commit=698ae47e6f3bae751850111d673bb418b80ef88e --suricata-commit=2058b2dc33249fa99158ee77815d66f7b32dc9ad --libhtp-commit=8a942c1c457bf4ca597caf1acaf08ef0a4171b8c -- --enable-testimony --enable-lua --enable-nfqueue

echo "Building Suricata with HASSH from Github"
./build.sh suricata-hassh --suricata-hassh-commit=6351ba81e440fa2a17188386865ddc93cc2f8f2d --libhtp-commit=8a942c1c457bf4ca597caf1acaf08ef0a4171b8c -- --enable-lua --enable-nfqueue

echo "Building Zeek from official repo"
./build.sh zeek 3.1.1

echo "Building Zeek-Testimony plugin from Github"
./build.sh zt --zeek-rpm=./out/zeek-3.1.1-0.x86_64.rpm --testimony-commit=77788166dc886779c3545c689a8a5fb1be8aed29 --zt-commit=4383e44e5ceb22c3386c3fee720ff74f3a18fe4e

# Create hashes list
(cd out && sha256sum *.rpm > hashes.list)

test_installation suricata
test_installation zeek
test_installation_zt

echo "Finished successfully!"
