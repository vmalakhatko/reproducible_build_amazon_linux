%define _buildhost reproducible
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}

#%define __requires_exclude ^.*stenographer.*$

Name:           suricata-stenographer
Version:        %{_version}
Release:        0

Summary:        Suricata-Stenographer plugin
License:        GPL-2.0-only
URL:            https://github.com/MalakhatkoVadym/suricata-stenographer-plugin
BuildRoot:      %{_tmppath}/%{name}-root
# Requires:       suricata
# BuildRequires:  suricata

%description
This plugin provides native Stenographer support for Suricata.
# Setup step is disabled because it requres source archive to be available.
# In case of the usage of git version it is pointless.
%prep
#%setup -q
%build

cd %{_builddir}

cd %{_builddir}/suricata-stenographer

echo "Building Suricata-Stenographer plugin"

# ./configure --install-root="${RPM_BUILD_ROOT}/%{_prefix}/lib/suricata/plugins/"

# find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

# cd build
CPPFLAGS="-I%{_builddir}/suricata/src" make -j`nproc`
cd -

%install

cd %{_builddir}/suricata-stenographer

# Install Suricata-Stenographer plugin
# cd build
make install
cd -

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/suricata-stenographer/files.list

%files -f %{_builddir}/suricata-stenographer/files.list


